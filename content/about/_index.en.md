+++
title = "About JoinJabber"
+++

JoinJabber is an informal international collective of individuals from diverse backgrounds with the common goal to promote the use of Jabber/XMPP, a free and federated social network with a focus on real-time chat applications.

If you're interested in our goals in more detail, you can find them [here](/about/goals/).
