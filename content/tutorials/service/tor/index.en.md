+++
title = "Tor access"
+++

### How can I federate my server with onion servers?

First, you should make sure [Tor](https://www.torproject.org/) is installed on your server. Then, there's two ways to go about it. Prosody server has excellent Tor/onion integration with [mod_onions](https://modules.prosody.im/mod_onions.html).

If you don't want to use this module, or are not using Prosody, you may consider configuring Tor as [a transparent proxy](https://gitlab.torproject.org/legacy/trac/-/wikis/doc/TransparentProxy). Tor will answer to DNS queries about onion services (for example, on port `5352`), announcing a record for a local address (for example in the `127.192.0.0/10` range), which iptables will forward through Tor's SOCKS5 proxy (for example, `127.0.0.1:9040`).

**Step 1:** Add to Tor config (usually `/etc/tor/torrc`):
```
VirtualAddrNetworkIPv4 127.192.0.0/10
VirtualAddrNetworkIPv6 [FE80::]/10
AutomapHostsOnResolve 1
TransPort 127.0.0.1:9040
TransPort [::1]:9040
DNSPort 5352
```
**Step 2:** Add in iptables config (usually `/etc/iptables/iptables.rules`):
```
-A OUTPUT -d 127.192.0.0/10 -p tcp -j REDIRECT --to-ports 9040
```
**Step 3**: Add in dnsmasq or other resolver's config (usually `/etc/dnsmasq.conf`)

Finally, configure your DNS resolver to use Tor's exposed transparent onion resolver as source of authority of ".onion" domain. For example, with dnsmasq you may simply add to `/etc/dnsmasq.conf`:
```
server=/onion/127.0.0.1#5352
```

