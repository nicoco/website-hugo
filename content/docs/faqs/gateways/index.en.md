+++
title = "Gateway FAQ"
weight = 4
+++

Jabber/XMPP gateways & bridges with other networks.

### Terminology

- "**Gateways**" or "**transports**" enable users to connect to other networks (IRC, Matrix...) as if they had joined it directly; the gateway will create a user session on the remote network in a process called "**puppeteering**";
- "**Bridges**" connect collective spaces (such as chatrooms) across networks without user intervention ; they can relay message through a dedicated user (relay bot) or by "**puppeteering**" users on remote networks. Some remote networks also allow the relay bot to temporarily change its name with is referred to as "**user spoofing**".

The terms gateway and bridge are sometimes used interchangeably. For example, on the Matrix network both are referred to as "bridges" but it makes a distinction between "portaled" (= gatewayed) and "plumbed" (= bridged) rooms.

### Limitations

- Due to the often reverse-engineered nature of such systems they are prone to breaking and commonly are in violation of the Terms of Service, especially on commercial networks like Discord;
- Due to the problems matching different protocol features, usability is nearly always degraded on both sides;
- Some networks ban publicly accessible gateways as they are easily abused by spammers and technically difficult to differentiate from automated spam-bots.

### Privacy & security 

- Due to exposing users messages to external networks the privacy of users is typically not preserved and rarely is it possible to acquire consent of all users;
- Malicious gateways and bridges may monitor the entirety of messages going through them, as network encryption (TLS) is terminated from both sides on the gateway itself;
- Credentials (such as passwords) used by gateways for login purposes are sometimes stored on the gateway server; in particular, for password-less systems like Telegram there may also be a risk of permanent account takeover when using public gateways. [2FA](https://en.wikipedia.org/wiki/Multi-factor_authentication) should be used in such cases;
- Some gateways publicly expose the user's JID to other networks, sometimes publicly, contrary to how most XMPP services operate (an XMPP chatroom admin can always see a user's JID, but usually regular users can't); this information is exposed to remote network by the gateway for abuse prevention (being able to distinguish between individual users);
- User-spoofing bridges can be easily abused to impersonate users.

### Gateway / bridge etiquette
- If you are the guest you have to play by the rules of the host. So if you for example use an XMPP -> IRC gateway you have to respect IRC conventions. But If you use a IRC -> XMPP gateway you have to respect XMPP conventions;
- Try to avoid using features that usually do not (reactions, threaded replies etc.) or only severely degraded (for example quoting) translate across gateway and bridges;
- Before setting up a bridge or gateway politely ask the channel operator or service owner if that is allowed (but in the case of commercial networks that might be impossible);
- In most cases gateways and bridges work only reliably when you are the service operator of both networks.

