+++
title = "Android"
template = "client.html"
[extra]
platform = "android"
clients = [ "conversations" ]
+++

Op Android is de aanbevolen client [Conversations](https://conversations.im). Je kunt Conversations downloaden op [F-Droid](https://f-droid.org/en/packages/eu.siacs.conversations/), de gratis app store voor Android. Als je F-Droid nog niet hebt geïnstalleerd, raden wij je ten zeerste aan om dat even te doen. Als je de ontwikkeling van Conversations wilt ondersteunen, kun je het kopen op Google's Play Store, of [doneren](https://conversations.im/#donate) aan het project.

