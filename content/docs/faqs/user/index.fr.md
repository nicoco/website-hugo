+++
title = "Foire aux questions"
template = "dummy_section.html"
+++

**La foire aux questions n'est pour l'instant disponible que [en anglais](@/faqs/_index.md)**. Toute aide pour traduire est bienvenue!
