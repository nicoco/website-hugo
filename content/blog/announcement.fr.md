+++
author = "JoinJabber"
tags = ["blog","announcement","collective"]
title = "Le collectif JoinJabber est né"
date = "2021-02-10"
image = "blog/undraw_announcement.svg"
+++

Dimanche dernier (07/02/2021), une réunion s'est tenue dans un salon public pour créer un nouveau collectif. Pour résumé, nous avons l'intention de créer une communauté orientée vers les attentes des personnes qui utilisent et administrent des services sur le réseau Jabber/XMPP. Le rapport de cette réunion peut être consulté [ici](@/collective/meeting-1/index.fr.md).

Si vous souhaitez participer à la prochaine réunion, merci de [voter pour une date sur notre forum](https://forum.joinjabber.org/t/meeting-2-poll-for-the-date/28/2). Elle aura lieu sur le salon [meeting@joinjabber.org](xmpp:meeting@joinjabber.org?join). L'ordre du jour de cette réunion est disponible [sur un pad](https://pad.lqdn.fr/p/joinjabber2). Tout le monde est invité à soumettre des idées avant ou au début de la réunion.

# Objectifs

Nos objectifs communs peuvent se résumer ainsi :

- fournir des informations à jour sur l'écosystème Jabber pour les personnes cherchant un client ou un fournisseur de service, ainsi que des lieux de support (forums et salons de discussion) dans le plus de langues possibles ;
- fournir des services de façon collective pour l'écosystème Jabber/XMPP, tel qu'une plateforme de traduction, un hébergement de site web, des forums/listes de diffusion, salons de discussion, etc. ;
- fournir des ressources à jour pour l'[auto-hébergement](https://fr.wikipedia.org/wiki/Auto-h%C3%A9bergement_(Internet)) d'un serveur Jabber/XMPP et la manière de le configurer pour offrir la meilleure expérience d'utilisation ;
- rassembler des retours d'expérience concernant les questions d'accessibilité, de vie privée, d'utilisabilité et d'interopérabilité sur différentes solutions, qui seraient sources de critiques constructives pour celles et ceux qui développent clients et serveurs, ou bien les administrent.

La liste complète de nos objectifs adoptés lors de la réunion peut être lue dans [le compte-rendu](@/collective/meeting-1/index.fr.md).

# Groupes de travail

Notre collectif utilise  l'anglais puisque c'était la langue commune lors de notre réunion. Pour se répartir les tâches, et faciliter l'accueil de nouvelles personnes, nous avons créé des groupes de travail :

- Sysadmin : déployer et maintenir les services sur notre infrastructure ;
- Site web : créer du contenu pour notre site web, et faire évoluer son design ;
- Traductions : coordonner les traductions et recruter de nouvelLEs participantEs ;
- Bridging : évaluer des solutions pour connecter plusieurs salons à travers différents réseaux (IRC, Matrix, Jabber/XMPP) ;
- Media : publier du contenu et rassembler les commentaires dans la section microblogging de notre site web, ainsi que sur divers réseaux sociaux (Mastodon, Movim, etc.). 

# Appel à contributions

JoinJabber est un collectif entièrement bénévole. Toute personne en accord avec nos [objectifs](@/collective/about/goals/index.fr.md) est la bienvenue pour contribuer à chaque axe de notre projet. Si tu ne sais pas comment participer, voici quelques idées.

## Tutoriels et autres contenus

Si tu as l'habitude d'utiliser un client particulier, ou d'administrer un serveur Jabber pour une communauté, tu peux écrire un tutoriel pour que d'autres s'y mettent. Tu peux également contribuer une FAQ.

## Traductions

Si tu veux aider aux traductions des ressources de JoinJabber dans ta langue, tu peux rejoindre le salon [traductions@joinjabber.org](xmpp:traductions@joinjabber.org?join).

## Conception visuelle (design)

Si tu peux aider à la conception visuelle (design), on pourrait utiliser :

- d'un logo, sous forme de bannière et de favicon carré ;
- d'aide pour la conception de notre site web, en évitant JavaScript, et sans l'allourdir ;
- d'aide pour la conception visuelle de clients Jabber/XMPP existants (GTK, Qt, ncurses, Android, iOS, web).


## Expérience d'utilisation (UX)

Si tu es impliquéE dans l'UX et les études d'utilisabilité, tu peux auditer les services déployés par notre collectif, les clients que nous recommandons, ainsi que les services additionnels mis en place par des opérateurs spécifiques, et faire des propositions pour améliorer les choses.

## Security

## Sécurité

Si tu es expertE en sécurité ou en tests d'intrusion (pentesting), tu peux nous aider en auditant :

- [notre infrastructure](https://codeberg.org/joinjabber/infra) ;
- des serveurs Jabber/XMPP et les configurations que nous recommendons dans divers tutoriels, ainsi que des solutions pré-empaquetées que nous mettons en avant tels que [Yunohost](https://yunohost.org/) et [Snikket](https://snikket.org/) ;
- [les clients Jabber/XMPP que nous recommandons](https://joinjabber.org/clients/) ;
- les fournisseurs de services que nous recommandons sur notre [page d'accueil](https://joinjabber.org/) .

## Administration système (sysadmin)

Si tu t'y connais en administration système de services auto-hébergés via Ansible (ou d'autres solutions d'infrastructure déclaratives et/ou programmables), tu peux contribuer à [notre infrastructure] (https://codeberg.org/joinjabber/infra).

Même si [Ansible](https://en.wikipedia.org/wiki/Ansible_(software)) c'est pas ton truc,
 tu peux auditer nos patrons (templates) de configuration pour :

- [nos services web](https://codeberg.org/joinjabber/infra/src/branch/main/roles/webserver/files) ;
- [nos services de courrier interne](https://codeberg.org/joinjabber/infra/src/branch/main/roles/mailserver/templates) ;
- [notre serveur de salons de discussion Jabber/XMPP](https://codeberg.org/joinjabber/infra/src/branch/main/roles/jabberserver/files/prosody.cfg.lua).

Tu peux trouver d'autres tickets liés à l'administration système sur [le bugtracker de notre infrastructure](https://codeberg.org/joinjabber/infra/issues).

