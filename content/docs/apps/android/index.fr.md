+++
title = "Android"
template = "client.html"
[extra]
platform = "android"
clients = [ "conversations" ]
+++

Sur Android, le client recommandé est [Conversations](https://conversations.im). Tu peux télécharger Conversations sur [F-Droid](https://f-droid.org/fr/packages/eu.siacs.conversations/), le magasin d'applications libres pour Android. Si tu n'as pas encore installé F-Droid, nous recommandons fortement de prendre le temps de le faire. Si tu veux soutenir le développement de Conversations, tu peux l'acheter sur le Google Play Store, ou bien [faire une donation au projet](https://conversations.im/#donate).

