+++
title = "Join Jabber"

+++
{{< block "grid-2" >}}
{{< column >}}

# Welcome to the JoinJabber community!

_An inclusive space on the Jabber network_

{{< tip "warning" >}}With Jabber you can [securely](docs/faqs/user/#faq-users-encryption) chat or call with your friends and family. Or you can join public group chats with people that share your interests, without having to share your personal data.
{{< /tip >}}

{{< tip >}}
Jabber, [also known as **XMPP**](docs/faqs/user/#faq-users-xmppjabber), is an _open standard_ for online communication. This means that the network belongs to all of us, not just to a single organization.

Jabber is [federated](docs/faqs/user/#faq-users-federation), just like email or Mastodon. Many servers connect together to create the Jabber network.
{{< /tip >}}

{{< tip "warning" >}}
On this page, we'll help you connect to Jabber in just two simple steps.

Furthermore, we invite you to join [our endeavor](about/goals/) to further expand the possibilities of the Jabber platform.
{{< /tip >}}

{{< button "docs/" "Let's get started" >}}{{< button "https://chat.joinjabber.org/" "Chat with us" >}}
{{< /column >}}

{{< column >}}
![](/images/undraw_texting.svg)
{{< /column >}}
{{< /block >}}
