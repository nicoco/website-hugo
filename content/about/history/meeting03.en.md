+++
author = "JoinJabber"
tags = ["meeting","minutes","collective"]
title = "Meeting #3"
date = "2023-01-23"
+++

Our third meeting took place online on January 27 2023 20:00 UTC, more than a
year after [the second one]({{< relref "/about/history/meeting02" >}}).

It has been a really slow few months at JoinJabber. Even though the general
chatroom was still up and people chatting on and off about various things,
nothing was really happening in the collective.

Things are moving in the community, be it about the revival of jabber.org, or
the preparations for long-awaited FOSDEM. We decided to create chatrooms for
user support, both end-users and server operators, to help new comers get in
without trouble.

With this possible new influx of users, we also decided to [adopt a Code of
Conduct](#CoC) for the collective.

Active participants: pep., msavoritias, root, singpolyma, Sam, Kris, Guillaume, diane

# Agenda {#agenda}

- [Next](#next)
- [Progress report](#progress)
    - [Collective tasks](#progress-collective)
    - [Sysadmin](#progress-sysadmin)
    - [Translations](#progress-translations)
    - [Bridging](#progress-bridging)
    - [Media](#progress-media)
- [Logo](#logo)
- [XMPP User Support](#support)
- [Initial Code of Conduct proposal][#CoC]
- [Channel pruning](#pruning)
- [Fosshost migration](#fosshost)
- [Website improvements](#website)
- [Promotion](#promo)
- [Extras](#extras)

## Next meeting {#next}

## Progress Report {#progress}

As mentioned in the intro, activity was sparse, so progress here is minimal
but not none!

### Collective tasks {#progress-collective}

- [ ] List other collectives we'd like to invite to the meeting here
- [ ] Prepare a tutorial on how to organize meetings and participate to them

No progress.

### Sysadmin {#progress-sysadmin}

- [x] configure prosody to support BOSH and anonymous login Anonymous webchat
  was setup and is working
- [ ] consider whether to enable mod_slack_webhooks so that matterbridge can
  create ghost users instead of relaying messages through a single nickname
  (highly experimental) Matterbridge is working but not really used,
  slackwebhooks is not being used and probably doesn't work anyways.
- [x] wait for more detailed matterbridge/ConverseJS/xmpp-web proposals and
  set them up see above, ConverseJS was trialed, but xmpp-web serves our
  purpose better

We now have Prosody expose Bosh and Websockets endpoints and [XMPP
Web](https://github.com/nioc/xmpp-web) is now deployed on
[chat.joinjabber.org](https://chat.joinjabber.org) with anonymous login to be
able to join our chatrooms easily.

Our current Ansible recipies have been designed in a way to allow sharing
among various projects. While collaboration is of course great, this was
maintained by a single person that isn't around anymore, and we don't have the
energy to keep it going.

It comes at a cost of being way too complex for our small infrastructure, and
we're looking to simplify and it keep the necessary pieces. The original
repository is still available, we've just merged roles we wanted to use in our
[infra](https://codeberg.org/joinjabber/infra) repository.

### Translations {#progress-translations}

- [ ] Recruit more translators
- [ ] Formulate a proposal for inclusive language for the next meeting
- [ ] Translate meeting minutes #1 and #2

No progress on these tasks.

Kris investigated whether the website could be more easily translated via a
i18n translation tool like Weblate. It seems difficult, but the KDE project
does it with their Hugo website.

### Bridging {#progress-bridging}

- [ ] Create matrix/IRC accounts with bridging@joinjabber.org recovery email
- [ ] Make a concrete proposal to the sysadmin team

No progress.

### Media {#progress-media}

- [ ] Create accounts on Mastodon and Movim, and share credentials
- [ ] Obtain credentials from the Website Working Group to publish on website
  in the microblogging section
- [ ] Consider whether we should selfhost our own Movim instance, and if so
  get in touch with the Sysadmin Working Group with a concrete proposal

No real progress on the microblogging part, but the week preceeding the
meeting we discussed potential Mastodon instances and contacted them.
indieweb.social already agreed to host us if we want, xmpp.social too (but
they need to set up an instance first).

## Logo (take 2) {#logo}

Tracking issue: https://codeberg.org/joinjabber/website/issues/51 (closed)

Line has proposed logos right about when we started but we didn't come to a decision at the time. For many time it has been asked if JoinJabber could have graphical elements for its website or chat rooms, so they have been resubmitted for discussion.

Participants choose to use a mix of F and I.

Files have now been uploaded to our [collective repository](https://codeberg.org/joinjabber/collective), where you can also find the appropriate licenses.

Thanks Line again for these logos. And thanks Guillaume for the modifications and all the icons used in many places already.

### XMPP User Support room {#support}

With FOSDEM coming up, and the jabber.org revival, it was asked in the XSF chatroom if a user support room already existed. As it had already been discussed a few times on JoinJabber chatrooms and people didn't seem closed to the idea, a support room was immediately created.

The room creation was acknowledged during the meeting. The meaning of “user” was not originally clear and when clarifying it we decided to split end-users and server operators. The first room is available at [support@joinjabber.org](https://chat.joinjabber.org/#/guest?join=support) ([xmpp](xmpp:support@joinjabber.org?join)), and the second one at [servers@joinjabber.org](https://chat.joinjabber.org/#/guest?join=servers) ([xmpp](xmpp:servers@joinjabber.org?join)).

Alongside these chatrooms we also opened a sort of “knowledge base” on our [Codeberg tracker](https://codeberg.org/joinjabber/support) to keep track of common issues in hope that they help users that come to our rooms. This knowledge base is meant to be used for both the “support” and the “servers” rooms.

With the possible influx of new users it was also acted to adopt a Code of Conduct.

#### Initial Code of Conduct proposal {#CoC}

A minimal Code of conduct was accepted during the meeting.

> No discrimination about race, sex, gender, ability, etc.  
> Focus on behavior not people when pointing out an issue.  
> Alt-right speech and trolling will not be tolerated.”

Work on a more detailed Code of conduct is happening and [a pad](https://pad.lqdn.fr/p/jj-coc) has been created. The original draft is heavily based on [kolektiva.social](https://kolektiva.social/about)'s work, but examples from the Python or Forgejo communities were also shared.

The proposed deadline is February 12th.

**Action**: Next meeting February 12th to decide on a more complete Code of Conduct.

### Channel pruning {#pruning}

Some of the chatrooms were never used and used to sit empty. It was decided to remove the most inactive ones and to create a single room for working groups called [project](https://chat.joinjabber.org/#/guest?join=project) ([xmpp](xmpp:project@joinjabber.org?join)).

The following rooms have been removed: *fr*, *media*, *meeting*, *sysadmin*, *translations*, *ux*, *website*.  
The following rooms are being kept: *abuse*, *bridging*, *chat*, *privacy*, *spaces*.  
The following rooms have been created: *project*, *support*, *servers*.

Pep. noted that some topics such as sysadmin may take space in the project chatroom, but agreed to remove the corresponding rooms.

The [“chat”](https://chat.joinjabber.org/#/guest?join=chat) ([xmpp](xmpp:chat@joinjabber.org?join)) room is to be used as a general Jabber/XMPP chat, alongside JoinJabber activities that address the larger community.

**Action**: None. Everything has been done right away.

### Migration from Fosshost {#fosshost}

Tracking issue: https://codeberg.org/joinjabber/infra/issues/23

Fosshost has announced that they were shutting down and that they would notify us. We need to figure out where to go next. People currently taking care of the infra didn't mind paying for a VPS and were more in favour of keeping control over services, while some others were more in favour of using mutualised services.

It was decided for now that we rent a VPS, and that the actual location be decided in the sysadmin group.

### Porting of website to Hugo and nicer looking theme {#website}

Tracking issue: https://codeberg.org/joinjabber/website/issues/50 (closed)  
Migrated website: https://codeberg.org/joinjabber/website-hugo

The current website is using Zola and a custom theme made by a contributor that hasn't been around for months. Some active contributors would prefer porting it over to an easier to use/maintain and nicer looking Hugo site.

It was decided to migrate the website to the new version when it is ready, which should happen before FOSDEM.

Easy language switching and German translation might not be ready just yet.

**Action**: Migrate the website by FOSDEM (DONE)  
**Action**: Sysadmins to add hugo support and webhooks

### Promotion: FOSDEM, AP surge, Unified Push {#promo}

Several exciting developments happened in 2022, both inside and outside the Jabber network.

We discussed shortly how the exciting influx of users to the ActivityPub federation (Mastodon) could benefit the Jabber network as well. Besides people learning about federation, the ActivityPub space currently has a very spotty user-story for direct messages (DMs), with either no or very limited implementations available. Currently there are project trying to reinvent the wheel, even with custom e2ee etc, but projects like [Akkoma](https://akkoma.dev/AkkomaGang/akkoma/) (a Pleroma fork) also recognized that ActivityPub really wasn't designed for private messaging let alone real-time chat and subsequently removed all such (low quality) implementations from their fork.

This presents a window of opportunity for XMPP that we should not just leave open for Matrix to occupy. There are already some guides and examples here and there how to integrate a XMPP server with a Mastodon or Pleroma instance (and other similar integrations), but maybe JoinJabber could add some easy to follow guides for Fediverse server admins to add a XMPP server and maybe also web-client to their instances. It was also suggested to open a promotional JoinJabber Mastodon account on an existing Fediverse instance.

Somewhat related there is now a renewed push for [Unified push](https://f-droid.org/en/2022/12/18/unifiedpush.html) (pun intended) with many Fediverse clients implementing client support, and Conversations / Prosody having two independent implementations of provider support. This means that Conversations can act as the single push notifications receiver without depending on Google's FCM. XMPP is uniquely placed for this as even FCM is using it internally and it was discussed how this could be great new XMPP feature promoted to the wider privacy conscious internet community. One proposal was to write a tutorial on how to set this up server side and what users need to do to enable it in their Mastodon clients etc.

Last but not least with [Libervia's](https://www.libervia.org/) ActivityPub XMPP gateway reaching a usable alpha status it was discussed if maybe JoinJabber should run a public gateway. For now it was agreed to wait for the official public test instance that the developer goffi is working on and maybe write a blog-post about it after testing it.

**Action**: Find a suitable 3rd party Mastodon instance and open an promotional account (DONE)  
**Action**: try to find contributors for writing various tutorials or blog-posts about all this? This was discussed, but no immediate volunteers.

### Extras {#extras}

Likely sun-setting of https://community.xmpp.net.

Free hosting offer from the Lemmy developers runs out in March 2023 and the uptake of the platform has been very low. Currently there seems to be no-one willing to step up and take over hosting.

No specific action for this topic, unless someone has an idea to keep it running and make it more popular.

It could be considered to add it to JoinJabber infra, but no strong support was expressed by participants.
