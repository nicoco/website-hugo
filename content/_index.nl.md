+++
title = "Join Jabber"

+++
{{< block "grid-2" >}}
{{< column >}}

# Word lid van Jabber

Een gratis & gedecentraliseerd sociaal netwerkplatform

{{< tip >}}
Welkom bij de Jabbergemeenschap. Op deze pagina helpen we je een server en een client te vinden om je aan te sluiten bij het Jabbernetwerk, zodat je kunt chatten met je vrienden zonder al je gegevens weg te geven. Jabber is gedecentraliseerd, net als e-mail. Dit betekent dat je een server moet vinden of maken, zodat je van daaruit kunt chatten met alle andere gebruikers.
{{< /tip >}}

{{< tip "warning" >}}
Dit kan in het begin verwarrend zijn, maar Jabber adressen (JIDs) zien er net zo uit als email adressen. Dus als je bijvoorbeeld de account `emma.goldman` hebt op server jabber.fr, zal je adres `emma.goldman@jabber.fr` zijn. Chatrooms (ook wel MUC's genoemd) hebben ook soortgelijke adressen, zoals `chat@joinjabber.org`.
{{< /tip >}}

{{< button "docs/" "Laten we beginnen!" >}}{{< button "https://chat.joinjabber.org/" "Chat" >}}
{{< /column >}}

{{< column >}}
![](/images/undraw_texting.svg)
{{< /column >}}
{{< /block >}}
