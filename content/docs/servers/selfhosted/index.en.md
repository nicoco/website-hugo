+++
title = "Self-hosted"
description = "Servers for self-hosting"
weight = 4
+++

It is also possible to run your own server. Here you will find some options for doing this. More details can be found in our [tutorials](/tutorials).

Homebrew Server club is also a nice place to [learn more](https://homebrewserver.club/category/instant-messaging.html).

We recommend using [Prosody](https://prosody.im/doc) or [Ejabberd](https://docs.ejabberd.im/) for hosting your own XMPP server. But these options make it a bit easier to get started:

## Snikket

[<img alt="Get started" src="/images/servers/snikket.svg" style="max-height:60px;height:100%">](https://snikket.org/service/quickstart/)

Snikket is a project started by one of the [Prosody](https://prosody.im) developers to provide a unified app experience including user friendly server hosting. Learn more about [their goals here](https://snikket.org/about/goals/).

## Yunohost

[<img alt="Get started" src="/images/servers/yunohost.svg" style="max-height:100px;height:100%">](https://yunohost.org)

YunoHost is an operating system aiming for the simplest administration of a server, in order to democratize self-hosting. It comes with a [pre-installed XMPP server](https://yunohost.org/en/XMPP) alongside other easy to install software for your server.

## Uberspace

[<img alt="Get started" src="/images/servers/uberspace.svg" style="max-height:100px;height:100%">](https://uberspace.de/en/)

If you prefer something in-between a managed service and running your own server or VPS, Uberspace has you covered. They provide a [detailed tutorial how to run your own Prosody XMPP service](https://lab.uberspace.de/guide_prosody/) on their shared server.
