+++
author = "JoinJabber"
tags = ["blog","website","collective"]
title = "New website design"
date = "2023-02-01"
image = "blog/undraw_website.svg"
+++

As discussed during our initial meeting in January 2023 ([minutes here](../../about/history/meeting03)) a new website design was drafted and the content from the old website mostly ported over to it. We also made the switch over the the static site generator Hugo to ease contributions a bit.

If you are reading this, we made the switch over to this new site and while things are still a bit of a work in progress, the important functionality of the old website should be mostly restored. Existing translations into Dutch, French and German are still somewhat broken, but we hope to improve that soon. We are also looking for help translating our website to Spanish and Portuguese.

If you are interested in contributing please join our [project chatroom](xmpp:project@joinjabber.org?join) and say hi :)
