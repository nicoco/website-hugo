+++
title = "FAQ"
description = "Frequently Asked Questions"
weight = 4
+++

Here you can find more detailed information about the platform:

- **[Users FAQ](/docs/faqs/user/)**: common questions for typical users
- **[Advanced FAQ](/docs/faqs/advanced/)**: more detailed questions about Jabber/XMPP
- **[Security FAQ](/docs/faqs/security/)**: specific security/privacy related questions
- **[Gateway FAQ](/docs/faqs/gateways/)**: questions about bridging to other services
- **[Server Admins FAQ](/docs/faqs/service/)**: common questions if you run your own server
