# JoinJabber website

Based on [Compose theme](https://github.com/onweru/compose). Find the documentation [here](https://docs.neuralvibes.com/docs/compose/install-theme/).

All text in the `content` folder is [Creative Commons CC-by-SA](https://creativecommons.org/licenses/by-sa/4.0/) licensed.

## Contributions

Contributions, especially tutorials and translations, are highly appreciated. This website uses plain-text files with simple [Markdown formatting](https://www.markdownguide.org/cheat-sheet/). Ready-made PRs or patch files for this repository are perfect and can be made quite easily on the Codeberg web-interface.
But if you send us a simple markdown formatted text file with your changes that is also fine.

You can join our project channel via a Jabber client to discuss any contributions with us: [project@joinjabber.org](xmpp:project@joinjabber.org?join) or you can also use our [simple webclient](https://chat.joinjabber.org/#/guest?join=project); no registration required.

Please note that all contributions should be licensed under the [Creative Commons CC-by-SA license](https://creativecommons.org/licenses/by-sa/4.0/) or a license compatible to it.

## Running a local copy of the site for editing

After cloning or downloading the git repository (and ensuring you have the [Hugo static site builder](https://gohugo.io/) installed) open a terminal and change to the `website-hugo` directory. Make sure you pull the git submodule for the theme first with `git submodule update --init --recursive`. Then you can start an interactive version of the site by running `hugo server`. The terminal output will show a link you can open with any web-browser. Changes to the files will be automatically reflected on this live-updating local version of the website.
