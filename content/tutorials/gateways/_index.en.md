+++
title = "Gateways"
+++

In the Jabber/XMPP ecosystem, gateways (transports) are the means to connect to different protocols via your client.

1. **[IRC](/tutorials/gateways/irc/)**
2. **[Matrix](/tutorials/gateways/matrix/)**
3. **[Slidge](/tutorials/gateways/slidge/)**

