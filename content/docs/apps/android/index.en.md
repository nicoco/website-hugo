+++
title = "Android"
+++

## [<img src="/images/apps/conversations.svg" style="max-height:30px;height:100%"> Conversations](/docs/apps/android/#conversations-android) {#conversations-android}

Our recommended client is [Conversations](https://conversations.im). You can download Conversations on [F-Droid](https://f-droid.org/en/packages/eu.siacs.conversations/), the free app store for Android. If you don't have F-Droid installed yet, we strongly recommend you take the time to set it up. If you want to support the development of Conversations, you can buy it on Google's Play Store, or [donate](https://conversations.im/#donate) to the project. Get it on:

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/en/packages/eu.siacs.conversations/)
[<img alt="Get it on Google Play" src="/images/apps/google.png" style="max-height:65px;height:100%">](https://play.google.com/store/apps/details?id=eu.siacs.conversations)

## [<img src="/images/apps/cheogram.svg" style="max-height:30px;height:100%"> Cheogram](/docs/apps/android/#cheogram-android) {#cheogram-android}

[Cheogram](https://cheogram.com/) is a fork of Conversations that adds some additional features. Get it on:

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/en/packages/com.cheogram.android/)

## [<img src="/images/apps/blabber.png" style="max-height:30px;height:100%"> Blabber.im](/docs/apps/android/#blabber-android) {#blabber-android}

[Blabber.im](https://blabber.im/en.html) is another fork of Conversations that aims to be easier to use. Get it [from their website](https://blabber.im/en.html) or on F-Droid.

_If you don't use F-Droid or want to make use of Google's push server for instant notifications, install the [Google Play APK](https://blabber.im/download-ps.php), instead of using a store. It will automatically look for updates once installed, so you won't miss security fixes or new features._

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/packages/de.pixart.messenger/)


