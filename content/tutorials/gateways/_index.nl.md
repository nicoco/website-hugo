+++
title = "Gateways"
+++

In het Jabber/XMPP-ecosysteem zijn "gateways/transports" de middelen om via je client verbinding te maken met andere protocollen.

1. **[IRC](/tutorials/gateways/irc/)**
2. **[Matrix](/tutorials/gateways/matrix/)**
3. **[Slidge](/tutorials/gateways/slidge/)**
